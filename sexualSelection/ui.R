# My first example
# 2014/Nov/04

library(shiny)

# FROM: http://stackoverflow.com/a/21132918 user Alex Brown
numericInputMod <- function (inputId, label, value = 0, class="input-mini", ...) 
{
  div(style="display:inline-block",
      tags$label(label, `for` = inputId), 
      tags$input(id = inputId, type = "number"
                 , value = value, class = class
                 , ... ))
}

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Effects of Anisogamy"),
  
  # Sidebar with a slider input for the number of bins
  sidebarLayout(
    sidebarPanel(
      numericInputMod("nMales",
                      "How many males?",
                      min = 1,
                      max = 100,
                      value = 10),
      
      numericInputMod("nFemales",
                      "How many females?",
                      min = 1,
                      max = 100,
                      value = 10),
      
      checkboxInput("simulate", "Should data be simulated?"
                    , value = FALSE),
      
      uiOutput("dataInputs")

    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      plotOutput("plotOutputs") #,
      #h3(textOutput("powerVal"), align = "center" )
    )
  )
))