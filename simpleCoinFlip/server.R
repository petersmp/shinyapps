library(shiny)

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
#   
#   input <- list()
#   input$trials <- 1000
#   input$flips <- 100
#   input$p <- 0.5
  
  output$distPlot <- renderPlot({
    # Sample
    nHeads <- unlist(lapply(1:input$trials, function(x){
      sum(sample( 1:0,
                  input$flips,
                  TRUE,
                  c(input$p, (1-input$p))))
      }))
    
    hist(nHeads,
         main = "Results of Simulations",
         xlab = "Number of heads",
         ylab = "Frequency")
    abline(v = input$p * input$flips, col = "red3", lwd = 4, lty = 3)
    legend("topright",
           legend = "Expected",
           col = "red3", lwd = 4, lty = 3)
  })
})