# Repository of shared Shiny applications

I am slowly moving my shiny applications from within
various projects to this central location.
Project histories have been left in their previous locations,
but future updates will be recorded here.

To access the running versions of these applications,
visit <http://petersonbiology.com/shiny>

## Contents


The useful applications that I have moved in are below.


- [Dragon genetic crosses](http://petersonbiology.com/shiny/simulatedGeneticCross/)
    - A series of Mendelian crosses that address
      dominance and the potential for interaction between two genes.
    - I am currenlty developing a more complete model that will include
      more advanced epistasis and linkage/crossing over
- [Statisitical power example](http://petersonbiology.com/shiny/powerExample/)
    - A graphical demonstration of statisitical power
- [Bayesian model selection example](http://petersonbiology.com/shiny/bayesModelSelect/)
    - A graphical demonstration of the weighting of two hypotheses
      given a single observed data value.
- [Coin Flip simulation](http://petersonbiology.com/shiny/simpleCoinFlip/)
    - Simulation showing the effects of sample size and random variation
- [Monty Hall simulation](http://petersonbiology.com/shiny/montyHallSimulator/)
    - A model that allows students to
      play the Monty Hall game (and track success)
      or simulate many plays
      to see the results of this non-intuitive logic question
- [NFC North simulation](http://petersonbiology.com/shiny/nflSimulator/)
    - First, allows students to simulate the number of wins for a particular team
    - In addition, allows a more complex model that includes the results
      of the other teams in the division
    - Demonstrates the difference between simple models
      (that can often be predicted by a simple equation)
      and more complex models that may be best solved by simulation.
- [Grade estimator](http://petersonbiology.com/shiny/simpleGradeCalculator/)
    - A tool to show predicted final grades
      (for a class based on points not percentages)
    - The defaults match the values in one of my classes,
      but it is flexible enough to adjust to most needs
    - I have a similar set up for classes based on percentages,
      but that is currently not flexible enough to be used
      for any other courses
	  (though I hope to make that one more flexible soon)
