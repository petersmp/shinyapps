# Dragon Cross
# 2015/Jan/03

library(shiny)

numericInputMod <- function (inputId, label, value = 0, class="input-mini", ...) 
{
  div(style="display:inline-block",
      tags$label(label, `for` = inputId), 
      tags$input(id = inputId, type = "number"
                 , value = value, class = class
                 , ... ))
}


# To make data frames work better
options(stringsAsFactors = FALSE)

# general global options
# Name teams and colors
teams <- c("GB","MIN","DET","CHI")
# Hex colors pulled from http://teamcolors.arc90.com/
teamColors <- c("#213D30","#4F2682","#006DB0","#DD4814")

# Play with colors
showpanel <- function(col)
{
  image(z=matrix(1:100, ncol=1), col=col, xaxt="n", yaxt="n" )
}
# showpanel(teamColors)

# # # for practice
# input <- list()
# input$whichModel <- "Simple"
# input$nSeasons <- 100
# input$wp <- 0.5
# input$nToWin <- 11
# input$GBwpMN <- 0.5
# input$GBwpDET <- 0.5
# input$GBwpCHI <- 0.5
# input$GBwpOTH <- 0.5
# input$MNwpDET <- 0.5
# input$MNwpCHI <- 0.5
# input$MNwpOTH  <- 0.5
# input$DETwpCHI <- 0.5
# input$DETwpOTH  <- 0.5
# input$CHIwpOTH  <- 0.5

# Define server logic
shinyServer(function(input, output) {
  
  # Set the choice options
  output$modelOptions <- renderUI({
    
    # ID other options
    if(input$whichModel == "Simple"){
      
      list(numericInput("wp",
                        "What is the probability that the Packers will win a given game?",
                        value = .5, min = 0, max = 1, step = 0.05),
           numericInput("nToWin",
                        "How many wins are required to win the division?",
                        11,0,16)
           )
      
    } else if(input$whichModel == "Complex"){
      
      list(
        h3("What is the probality the Packers will beat each team:"),
        numericInputMod("GBwpMN",
                     "Vikings",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("GBwpDET",
                     "Lions",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("GBwpCHI",
                     "Bears",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("GBwpOTH",
                     "Non-division teams",
                     value = .5, min = 0, max = 1, step = 0.05),
        h3("What is the probality the Vikings will beat each team:"),
        numericInputMod("MNwpDET",
                     "Lions",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("MNwpCHI",
                     "Bears",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("MNwpOTH",
                     "Non-division teams",
                     value = .5, min = 0, max = 1, step = 0.05),
        h3("What is the probality the Lions will beat each team:"),
        numericInputMod("DETwpCHI",
                     "Bears",
                     value = .5, min = 0, max = 1, step = 0.05),
        p(""), 
        numericInputMod("DETwpOTH",
                     "Non-division teams",
                     value = .5, min = 0, max = 1, step = 0.05),
        h3("What is the probality the Bears will beat each team:"),
        numericInputMod("CHIwpOTH",
                     "Non-division teams",
                     value = .5, min = 0, max = 1, step = 0.05)
      )
    }
  })
  
  # Run the simulation in a reactive variable to sync all others
  runSim <- reactive({
    
    # set reactives?
    # It doesn't seem like I should have to
    # This fixed the issue -- for some reason it wasn't finding these reactives if buried
    !is.null(input$wp)
    # !is.null(input$nToWin) # Not needed to run the actual simulation
    !is.null(input$whichModel)
    !is.null(input$GBwpMN)
    !is.null(input$GBwpDET)
    !is.null(input$GBwpCHI)
    !is.null(input$GBwpOTH)
    !is.null(input$MNwpDET)
    !is.null(input$MNwpCHI)
    !is.null(input$MNwpOTH)    
    !is.null(input$DETwpCHI)
    !is.null(input$DETwpOTH)    
    !is.null(input$CHIwpOTH)    
    !is.null(input$nToWin)
    
    if(input$whichModel == "Simple" & !is.null(input$wp)){
      # Run the simple simulation
      
      packerWins <- unlist(lapply(1:input$nSeasons, function(x){
        sum(sample(1:0,
                   16,
                   TRUE,
                   c(input$wp, 1 - input$wp ) )
            )
        }))
      
      # Always return a list for this to hold other things
      out <- list(packerWins = packerWins
                  , packWinDiv = packerWins >= input$nToWin)
      return(out)
      
    } else if(input$whichModel == "Complex" & !is.null(input$GBwpMN)) {
      # Run the complex model
      
      tempOut <- do.call(rbind, lapply(1:input$nSeasons, function(x){
        
        # Set the winners for the intra-division games
        # TRUE is won by first team listed
        # FALSE is won by second team listed
        
        GBvMN  <- sample( c(TRUE,FALSE), 2, TRUE, c(input$GBwpMN,  1 - input$GBwpMN) )
        GBvDET <- sample( c(TRUE,FALSE), 2, TRUE, c(input$GBwpDET, 1 - input$GBwpCHI) )
        GBvCHI <- sample( c(TRUE,FALSE), 2, TRUE, c(input$GBwpCHI, 1 - input$GBwpCHI) )
        
        MNvDET <- sample( c(TRUE,FALSE), 2, TRUE, c(input$MNwpDET, 1 - input$MNwpCHI) )
        MNvCHI <- sample( c(TRUE,FALSE), 2, TRUE, c(input$MNwpCHI, 1 - input$MNwpCHI) )
        
        DETvCHI <- sample( c(TRUE,FALSE), 2, TRUE, c(input$DETwpCHI, 1 - input$DETwpCHI) )
        
        # Set each teams record
        # The added zeros are for the "location" of games vs self for the tie breakers
        wins <- list()
        wins[["GB"]]  <- as.numeric(c(0, 0, GBvMN, GBvDET, GBvCHI,
                                      sample( c(TRUE,FALSE), 10, TRUE,
                                              c(input$GBwpOTH, 1 - input$GBwpOTH)   ))  ) 
        wins[["MIN"]] <- as.numeric(c(!GBvMN, 0, 0, MNvDET, MNvCHI,
                                      sample( c(TRUE,FALSE), 10, TRUE,
                                              c(input$MNwpOTH, 1 - input$MNwpOTH)   )) )
        wins[["DET"]] <- as.numeric(c(!GBvDET, !MNvDET, 0, 0, DETvCHI,
                                      sample( c(TRUE,FALSE), 10, TRUE,
                                              c(input$DETwpOTH, 1 - input$DETwpOTH) )) )
        wins[["CHI"]] <- as.numeric(c(!GBvCHI, !MNvCHI, !DETvCHI, 0, 0,
                                      sample( c(TRUE,FALSE), 10, TRUE,
                                              c(input$CHIwpOTH, 1 - input$CHIwpOTH) )) )
        nWins <- unlist(lapply(wins,sum))
        # How many wins for division winner(s)
        nWinsWinner <- max(nWins)
        
        # Set division winner (not using which.max in case of ties)
        winners <- names(nWins)[nWins == nWinsWinner]
        
        if(length(winners) == 1){
          divisionWinners <- winners
        } else {
          # Deal with ties
          # Only count wins in games between tied opponents
          limWins <- unlist(lapply( wins[winners], function(x){
            sum(x[which(rep(teams %in% winners, each = 2))])
            }))
          tempWinners <- names(limWins)[limWins == max(limWins)]
          
          # IF that broke tie
          if(length(tempWinners) == 1){
            divisionWinners <- tempWinners
          } else{
            # Count division record
            limWins <- unlist(lapply( wins[winners], function(x){
              sum(x[1:8])
            }))
            tempWinners <- names(limWins)[limWins == max(limWins)]
            
            # IF that broke tie
            if(length(tempWinners) == 1){
              divisionWinners <- tempWinners
            } else{
              # Skipping other tie breakers, as they may not be possible in current setup
              # Jumping straight to drawing lots
              divisionWinners <- sample(tempWinners, 1)
            }
          }
        }
        
        # Set up the output
        internalOut <- list(packerWins = nWins["GB"],
                            divisionWinners = divisionWinners,
                            nWinsWinner = nWinsWinner
                            , packWinDiv = divisionWinners == "GB")
        return(internalOut)
        }) # end lapply
      ) # end do.call
      
      # Return as named list
      out <- list( packerWins      = unlist(tempOut[,"packerWins"]),
                   divisionWinners = unlist(tempOut[,"divisionWinners"]),
                   nWinsWinner     = unlist(tempOut[,"nWinsWinner"]),
                   packWinDiv      = unlist(tempOut[,"packWinDiv"]))
        
      return(out)
      
    } else{
      out <- list("ERROR: No proper model selected")
      return(out)
    }
  })
  
  
  # Make a plot for the packer wins, either way
  output$plotPackers <- renderPlot({
    
    # Make reactive and sync timing
    out <- runSim()
    !is.null(input$nToWin)
    
    # Plot packer wins
    if(!is.null(input$wp) | !is.null(input$GBwpMIN)){
      tempTable <- table( factor(out$packerWins,0:16)
                          , factor(out$packWinDiv
                                   , levels = c(TRUE, FALSE)
                                   , labels = c("Yes", "No")))
      loc <- barplot(t(tempTable),
                     ylab = "Number of seasons in simulation",
                     xlab = "Number of wins for the Packers"
                     , legend.text = TRUE
                     , args.legend = list(x = "topleft"
                                          , inset = c(0.01,-0.15)
                                          , xpd = TRUE
                                          , horiz = TRUE
                                          , title = "Packers win the division?"))
      
      if(input$whichModel == "Simple"){
        # abline(v = mean(loc[input$nToWin:(input$nToWin+1)]),
               # col = "red3", lwd = 3, lty = 3
        # )
      
        predicted <- unlist(lapply(0:16,function(x){
          input$nSeasons * choose(16, x) * input$wp^x * (1-input$wp)^(16-x)
          }))
        lines(loc, predicted, col = "blue", lwd = 2, xpd = TRUE)
        
        # catch <- lapply(0:16,function(x){
        #   lines(c(mean(loc[c(x+0,x+1)]), mean(loc[c(x+1,x+2)])),
        #         rep(
        #           ,2) )
        # })
        
      }      
    }

  })
  
  # Print messages
  output$textCurrent <- renderText({
    out <- runSim()
    !is.null(input$nToWin)
    
    if(input$whichModel == "Simple" & !is.null(input$wp) ){
      madePlayoffs <- out$packerWins >= input$nToWin
      
      # Make the text
      temp <- paste(
        "In our simulation of ", input$nSeasons, " seasons, ",
        "the Packers won enough games (", input$nToWin," or more, the red line on the graph) ",
        "to make the playoffs in ", sum(madePlayoffs), " seasons ",
        "(", round(mean(madePlayoffs)*100,2),"% of the time). ",
        "The Packers won an average of ", round(mean(out$packerWins),2), " games per season ",
        "equivalent to a ", round(mean(out$packerWins)/16,3)," winning proportion.",
        "Note the similarity between the simulated results (gray bars) ",
        "and the results predicted by a simple mathematical model (blue line; the binomial distribution)."
        ,sep = "")
      return(temp)
    } else if(input$whichModel == "Complex" & !is.null(input$GBwpMN)   ){ #& !is.null(input$GBwpMIN)){
      temp <- paste(
        "In our simulation of ", input$nSeasons, " seasons, ",
        "the Packers won enough games (", "more than other teams in the division) ",
        "to make the playoffs in ", sum(out$divisionWinners == "GB"), " seasons ",
        "(", round(mean(out$divisionWinners == "GB")*100,2),"% of the time). ",
        "The Packers won an average of ", round(mean(out$packerWins),2), " games per season ",
        "equivalent to a ", round(mean(out$packerWins)/16,3)," winning proportion. ",
        "In addition, the division was won ",
        sum(out$divisionWinners == "MIN"), " times by the Vikings, ",
        sum(out$divisionWinners == "DET"), " times by the Lions, and ",
        sum(out$divisionWinners == "CHI"), " times by the Bears."
        ,sep = "")
    } else{
      temp <- "Please set the model parameters in the left panel."
      #temp <- paste(input$whichModel,!is.null(input$GBwpMIN))
      return(temp)
    }
    
    
    # For testing:
    # out <- paste(out, gameState, currDoor, didSwitch)
    # return(out)
  })
  
  # Make a plot for the division winners
  output$plotDivWinners <- renderPlot({
    
    # Make reactive and sync timing
    out <- runSim()
    
    
    # Plot division winners
    if(input$whichModel == "Complex" & !is.null(input$GBwpMN) ){ #& !is.null(input$GBwpMIN)){
      tempTableWin <- table( factor(out$divisionWinner,teams))
      tempTableN  <- table(out$nWinsWinner)
      
      par(mfrow = c(1,2))
      
      barplot(tempTableWin,
              col = teamColors,
              ylab = "Number of seasons division won",
              xlab = "Team")
      barplot(tempTableN,
              ylab = "Number of seasons",
              xlab = "Games won by division winner")
      
    }      
    
  })
  

})