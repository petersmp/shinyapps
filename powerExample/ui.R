# My first example
# 2014/Nov/04

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Demonstrate Power"),
  
  # Sidebar with a slider input for the number of bins
  sidebarLayout(
    sidebarPanel(
      numericInput("pThresh",
                   "The threshold to use:",
                   min = 0,
                   max = 0.1,
                   value = 0.05),
      
      sliderInput("alt",
                  "How different is the actual value?",
                  min = -10,
                  max =  10,
                  value = 2),
      
      sliderInput("mySD",
                  "The population sd:",
                  min = 1,
                  max = 10,
                  value = 2),
      
      sliderInput("myN",
                  "The sample size:",
                  min = 1,
                  max = 100,
                  value = 20)
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      plotOutput("distPlot"),
      h3(textOutput("powerVal"), align = "center" )
    )
  )
))