# My first example
# 2014/Nov/04

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Bayes model selection"),
  
  # Sidebar with a slider input for the number of bins
  sidebarLayout(
        
    sidebarPanel(
    
      h3("Model A"),
      
      numericInput("meanA",
                  "Mean:",
                  min = 0,
                  max = 1000,
                  value = 50),
      
      numericInput("sdA",
                   "Population standard deviation:",
                   min = 0,
                   max = 1000,
                   value = 5),
      
      numericInput("priorA",
                   "Prior Probability:",
                   min = 0,
                   max = 1,
                   value = 0.5),
      
      h3("Model B"),
      
      numericInput("meanB",
                   "Mean:",
                   min = 0,
                   max = 1000,
                   value = 55),
      
      numericInput("sdB",
                   "Population standard deviation:",
                   min = 0,
                   max = 1000,
                   value = 5),
    
      h3("Data information"),
      
      numericInput("theData",
                   "What value was observed?",
                   min = 0,
                   max = 1000,
                   value = 60),
      
      textInput(   "dataName",
                   "What is being measured?",
                   value = "Parameter (Units)")
      
      ),
      
    # Show a plot of the generated distribution
    mainPanel(
      plotOutput("distPlot"),
      h3(textOutput("bayesCalcData"), align = "center" ),
      h3(textOutput("bayesCalcPost"), align = "center" ),
      plotOutput("bayesCalc", height = "40px"),
      plotOutput("bayesCalcGeneric", height = "40px")
    )
  )
))